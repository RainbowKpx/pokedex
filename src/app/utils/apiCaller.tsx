import axios from 'axios'

const http = axios.create({})

export const apiGet = (url: string) => {
  return http.get(url).then((response: any) => response)
}
