import React, { FunctionComponent, useState } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { Menu } from 'antd'

const NavMenu: FunctionComponent = () => {
  const location = useLocation()
  const [current, setCurrent] = useState(filterKeys(location.pathname))

  const handleClick = (e: { key: React.SetStateAction<string> }) => {
    setCurrent(filterKeys(e.key.toString()))
  }

  function filterKeys(pathname: string) {
    var finalKey = pathname

    if (finalKey.toString().match('/Pokedex*')) finalKey = '/Pokedex'

    return finalKey
  }

  return (
    <Menu
      onClick={handleClick}
      selectedKeys={[current]}
      mode="horizontal"
      theme="dark"
    >
      <Menu.Item key="/">
        <Link to="/">Home</Link>
      </Menu.Item>
      <Menu.Item key="/Pokedex">
        <Link to="/Pokedex">Pokedex</Link>
      </Menu.Item>
      {process.env.REACT_APP_ENV && process.env.REACT_APP_ENV == 'development' && (
        <>
          <Menu.Item key="/SamplePage">
            <Link to="/SamplePage">SamplePage</Link>
          </Menu.Item>
          <Menu.Item key="/TestPage">
            <Link to="/TestPage">TestPage</Link>
          </Menu.Item>
        </>
      )}
    </Menu>
  )
}

export default NavMenu
