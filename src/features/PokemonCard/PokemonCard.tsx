import React, { FunctionComponent, useState } from 'react'
import { Col, Row } from 'antd/lib/grid'
import { Link } from 'react-router-dom'
import { useGetPokemonByNameQuery } from '../../app/services/Pokemon'
import { Avatar, Button, Spin } from 'antd'
import { StarOutlined, StarFilled } from '@ant-design/icons'

import './PokemonCard.css'

interface PokemonCardProps {
  name: string
}

const PokemonCard: FunctionComponent<PokemonCardProps> = (
  props: PokemonCardProps,
) => {
  const { name } = props
  const { data, error, isLoading } = useGetPokemonByNameQuery(name)

  const [isShiny, setIsShiney] = useState(false)

  if (isLoading) {
    return <Spin tip="Loading..." />
  }

  if (error) {
    return <span>Error: {error}</span>
  }

  return (
    <Col
      xs={{ span: 10, offset: 1 }}
      md={{ span: 6, offset: 0 }}
      lg={{ span: 4, offset: 0 }}
    >
      <div className="pokemonCard">
        <Row align="middle" justify="space-around">
          <Col>
            {isShiny && (
              <Avatar shape="square" size={96} src={data.sprites.front_shiny} />
            )}
            {!isShiny && (
              <Avatar
                shape="square"
                size={96}
                src={data.sprites.front_default}
              />
            )}
          </Col>
        </Row>
        <Row align="middle" justify="space-around">
          <Col>
            <Button
              onClick={() => {
                setIsShiney(!isShiny)
              }}
            >
              {isShiny && <StarFilled />}
              {!isShiny && <StarOutlined />}
            </Button>
          </Col>
        </Row>
        <Row align="middle" justify="space-around">
          <Col>
            <Link to={'/Pokedex/' + name}>
              <Button type="link" onClick={() => {}}>
                {name[0].toUpperCase() + name.substring(1)}
              </Button>
            </Link>
          </Col>
        </Row>
      </div>
    </Col>
  )
}

export default PokemonCard
