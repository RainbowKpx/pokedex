import React, { FunctionComponent } from 'react'
import { Col, Row } from 'antd/lib/grid'
import PokemonCard from '../PokemonCard/PokemonCard'
import { PageHeader } from 'antd'
import { useGetPokemonByNameQuery } from '../../app/services/Pokemon'

interface Pokemon {
  name: string
}

const PokedexPage: FunctionComponent = () => {
  const ALL_POKEMON = 898
  const limit = 151

  const { data, error, isLoading } = useGetPokemonByNameQuery('?limit=9')

  if (isLoading) {
    return <span>Loading...</span>
  }

  if (error) {
    return <span>Error: {error}</span>
  }

  return (
    <Row>
      <Col
        xs={{ span: 22, offset: 1 }}
        md={{ span: 20, offset: 2 }}
        lg={{ span: 18, offset: 3 }}
      >
        <Row>
          <PageHeader title="Pokedex" subTitle="Gotta catch em all !" />
        </Row>
        <Row gutter={[20, 20]}>
          {data.results.map((res: Pokemon, index: number) => {
            return <PokemonCard key={index} name={res.name} />
          })}
        </Row>
      </Col>
    </Row>
  )
}

export default PokedexPage
