import React, { FunctionComponent } from 'react'
import { Col, Row } from 'antd/lib/grid'
import { PageHeader } from 'antd'

const HomePage: FunctionComponent = () => {
  return (
    <Row>
      <Col
        xs={{ span: 22, offset: 1 }}
        md={{ span: 20, offset: 2 }}
        lg={{ span: 18, offset: 3 }}
      >
        <PageHeader title="Homepage" />
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </Col>
    </Row>
  )
}

export default HomePage
