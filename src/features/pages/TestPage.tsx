import React, { FunctionComponent } from 'react'
import { Col, Row } from 'antd/lib/grid'
import { PageHeader } from 'antd'

const TestPage: FunctionComponent = () => {
  return (
    <Row>
      <Col
        xs={{ span: 22, offset: 1 }}
        md={{ span: 20, offset: 2 }}
        lg={{ span: 18, offset: 3 }}
      >
        <PageHeader title="TestPage" />
      </Col>
    </Row>
  )
}

export default TestPage
