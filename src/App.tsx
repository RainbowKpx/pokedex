import React, { FunctionComponent } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css'
import 'antd/dist/antd.css'
import NavMenu from './features/NavMenu/NavMenu'
import HomePage from './features/pages/HomePage'
import SamplePage from './features/pages/SamplePage'
import PokedexPage from './features/pages/PokedexPage'
import TestPage from './features/pages/TestPage'
import Layout, { Content, Footer, Header } from 'antd/lib/layout/layout'
import { QueryClient, QueryClientProvider } from 'react-query'

const App: FunctionComponent = () => {
  const queryClient = new QueryClient()

  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <Layout>
          <Header>
            <NavMenu />
          </Header>
          <Content>
            <Switch>
              <Route path="/Pokedex">
                <PokedexPage />
              </Route>
              <Route path="/SamplePage">
                <SamplePage />
              </Route>
              <Route path="/TestPage">
                <TestPage />
              </Route>
              <Route exact path="/">
                <HomePage />
              </Route>
            </Switch>
          </Content>
          <Footer>Footer</Footer>
        </Layout>
      </Router>
    </QueryClientProvider>
  )
}

export default App
